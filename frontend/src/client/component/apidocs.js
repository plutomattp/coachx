var React = require('react');
var request = require('superagent');


var ApiDocs = React.createClass({
	getRequestBody: function(apidoc) {
		if (apidoc.exampleBody != "N/A") {
			return (
				<div>
					<p className='response'>Example request body:</p>
					<div className='json'><pre><code>{ apidoc.exampleBody }</code></pre></div>
				</div>
			);
		} else {
			return <div></div>;
		}
	},

	getInitialState: function() {
		var thisComponent = this;

		setTimeout(
			function() {
				request
				.get("/apidocumentation")
				.set({'Content-Type': 'application/json'})
				.end(function(err, res) {
					res.body.forEach(function(apidoc) {
						var current = thisComponent.state.apidocs;
						current.push(
							<div key={ apidoc.path }>
								<div className='path'>{ apidoc.httpMethod + " " + apidoc.path }</div>
								<div className='description'>{ apidoc.description }</div>
								<div> { thisComponent.getRequestBody(apidoc) } </div>
								<div>
									<p className='response'>Example response:</p>
									<div className='json'><pre><code>{ apidoc.exampleResponse }</code></pre></div>
								</div>
								<hr/>
							</div>
						);
						thisComponent.setState({
							apidocs: current
						});
					});
				});
			},
			0
		);

		return {
			apidocs: []
		}
	},

	render: function() {
		return (
			<div id='apidocs'>{ this.state.apidocs }</div>
		);
	}
});

module.exports = ApiDocs;