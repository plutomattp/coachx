var React = require('react');
var ReactDOM = require('react-dom');
var ApiDocs = require('./component/ApiDocs');

var Application = React.createClass({
	render: function() {
		return  (
			<div>
				<h1>API Documentation</h1>
				<hr/>
				<ApiDocs></ApiDocs>
			</div>
		);
	}
});

ReactDOM.render(
	<Application />, 
	document.getElementById("application")
);