package com.thecoachx.coachx.frontend;

/**
 * Configuration for the frontend
 * 
 * @author Matt
 */
public class FrontendConfiguration {

	private String staticFileDirectory;
	
	/**
	 * @return the directory where the static files are stored
	 */
	public String getStaticFileDirectory() {
		return staticFileDirectory;
	}
}