package com.thecoachx.coachx.frontend;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestEndpoint;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;

import spark.Route;

/**
 * Internal GET endpoint to display documentation
 * 
 * @author Matt
 */
public class GetApiDocumentation implements RestGetEndpoint {

	private Set<RestEndpoint> endpoints;

	@Inject
	GetApiDocumentation(Set<RestEndpoint> endpoints) {
		this.endpoints = endpoints;
	}
	
	
	@Override
	public String getPath() {
		return "/apidocumentation";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			List<ApiDocumentationResponse> response = new LinkedList<>();
			
			endpoints.forEach(endpoint -> {
				Documentation documentationAnnotation = endpoint.getClass().getAnnotation(Documentation.class);
				
				if (documentationAnnotation.exclude()) {
					return;
				}
				
				response.add(
					new ApiDocumentationResponse(
						endpoint.getHttpMethod(),
						endpoint.getPath(),
						documentationAnnotation.description(),
						documentationAnnotation.exampleBody(),
						documentationAnnotation.exampleResponse()
					)
				);
			});
			
			return new Gson().toJson(response);
		};
	}
	
	
	static class ApiDocumentationResponse {
		public final String httpMethod;
		public final String path;
		public final String description;
		public final String exampleResponse;
		public final String exampleBody;
		
		public ApiDocumentationResponse(String httpMethod, String path, String description, String exampleBody, String exampleResponse) {
			this.httpMethod = httpMethod;
			this.path = path;
			this.description = description;
			this.exampleBody = exampleBody;
			this.exampleResponse = exampleResponse;
		}
	}
}