package com.thecoachx.coachx.frontend;

import static spark.Spark.get;
import static spark.SparkBase.staticFileLocation;

import com.google.inject.Inject;

/**
 * Registers the frontend of the web application
 * 
 * @author Matt
 */
public class FrontendRegistrationService {
	
	private FrontendConfiguration frontendConfiguration;

	@Inject
	FrontendRegistrationService(FrontendConfiguration frontendConfiguration) {
		this.frontendConfiguration = frontendConfiguration;
	}
	

	/**
	 * Registers the frontend
	 */
	public void register() {
		staticFileLocation(frontendConfiguration.getStaticFileDirectory());
		
		get("/", (req, res) -> {
			return page();
		});
		
		
		get("/apidocs", (req, res) -> {
			return apipage();
		});
	}


	private String apipage() {
		return "<!DOCTYPE html><html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"api.css\"></head><body><div id=\"application\"></div><script src=\"client-bundle-ugly.js\"></script></body></html>";
	}


	private String page() {
		return "<!DOCTYPE html><html><head><link href='https://fonts.googleapis.com/css?family=Raleway:400, 600' rel='stylesheet' type='text/css'><link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"></head><body><img src='logo.png'></body></html>";
	}
}