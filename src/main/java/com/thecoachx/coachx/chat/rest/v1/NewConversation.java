package com.thecoachx.coachx.chat.rest.v1;

import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.ConversationAccess;
import com.thecoachx.coachx.core.persistence.schema.ChatMessage;
import com.thecoachx.coachx.core.persistence.schema.Conversation;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestPostEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */ 
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="A simple REST request to request to create a Conversation",
	exampleResponse="{\n  applicationStatus=\"OK\"\n}",
	exclude=true
)
public class NewConversation implements RestPostEndpoint {
	
	private ConversationAccess conversationAccess;

	@Inject
	NewConversation(ConversationAccess conversationAccess) {
		this.conversationAccess = conversationAccess;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/newConversation";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			JsonObject fromJson = new Gson().fromJson(req.body(), JsonObject.class);
			
			Conversation newConversation = new Conversation();
			newConversation.setConversationId(new Random().nextLong());
			for (JsonElement p : fromJson.get("participants").getAsJsonArray()) {
				newConversation.getParticipants().add(p.getAsNumber().longValue());
			}
			
			ChatMessage chatMessage = new ChatMessage();
			chatMessage.setMessage(fromJson.get("firstMessage").getAsJsonObject().get("message").getAsString());
			chatMessage.setSenderId(fromJson.get("firstMessage").getAsJsonObject().get("senderId").getAsNumber().longValue());
			chatMessage.setTimeStamp(System.currentTimeMillis());
			
			newConversation.getMessages().add(chatMessage);
			conversationAccess.insert(newConversation);
			
			return Long.toString(newConversation.getConversationId());
		};
	}
}