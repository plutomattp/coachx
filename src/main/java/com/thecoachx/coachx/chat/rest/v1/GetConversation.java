package com.thecoachx.coachx.chat.rest.v1;

import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.ConversationAccess;
import com.thecoachx.coachx.core.persistence.schema.ChatMessage;
import com.thecoachx.coachx.core.persistence.schema.Conversation;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="A simple REST request to request for all messages of a Conversation",
	exampleResponse="{\n  applicationStatus=\"OK\"\n}",
	exclude=true
)
public class GetConversation implements RestGetEndpoint {
	
	private ConversationAccess conversationAccess;

	@Inject
	GetConversation(ConversationAccess conversationAccess) {
		this.conversationAccess = conversationAccess;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/conversation/:conversationid";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			long conversationid;
			
			try {
				conversationid = Long.parseLong(req.params(":conversationid"));
			} catch (Exception e) {
				return "Invalid conversation ID";
			}
			
			Optional<Conversation> conversation = conversationAccess.findById(conversationid);
			
			if (conversation.isPresent()) {
				JsonObject jsonElement = new JsonObject();
				JsonArray participants = new JsonArray();
				JsonArray messages = new JsonArray();
				
				for (long participant : conversation.get().getParticipants()) {
					participants.add(participant);
				}
				for (ChatMessage message : conversation.get().getMessages()) {
					JsonObject messageJson = new JsonObject();
					messageJson.add("sender", new JsonPrimitive(message.getSenderId()));
					messageJson.add("message", new JsonPrimitive(message.getMessage()));
					messageJson.add("timestamp", new JsonPrimitive(message.getTimeStamp()));
					messages.add(messageJson);
				}
				
				jsonElement.add("participants", participants);
				jsonElement.add("messages", messages);
				return new Gson().toJson(jsonElement);
			} else {
				return "No conversation found with id " + conversationid;
			}
		};
	}
}