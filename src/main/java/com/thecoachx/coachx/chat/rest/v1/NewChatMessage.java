package com.thecoachx.coachx.chat.rest.v1;

import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.ConversationAccess;
import com.thecoachx.coachx.core.persistence.schema.ChatMessage;
import com.thecoachx.coachx.core.persistence.schema.Conversation;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestPostEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */ 
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="A simple REST request to request to create a ChatMessage",
	exampleResponse="{\n  applicationStatus=\"OK\"\n}",
	exclude=true
)
public class NewChatMessage implements RestPostEndpoint {
	
	private ConversationAccess conversationAccess;

	@Inject
	NewChatMessage(ConversationAccess conversationAccess) {
		this.conversationAccess = conversationAccess;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/newChatMessage";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			JsonObject fromJson = new Gson().fromJson(req.body(), JsonObject.class);

			Optional<Conversation> conversation = conversationAccess.findById(fromJson.get("conversataionId").getAsNumber().longValue());
			
			if (conversation.isPresent()) {
				ChatMessage chatMessage = new ChatMessage();
				chatMessage.setSenderId(fromJson.get("senderId").getAsNumber().longValue());
				chatMessage.setMessage(fromJson.get("message").getAsString());
				chatMessage.setTimeStamp(System.currentTimeMillis());
				conversationAccess.addMessage(conversation.get().getConversationId(), chatMessage);
				
				return Long.toString(conversation.get().getConversationId());
			} else {
				return "Conversation not found";
			}
		};
	}
}