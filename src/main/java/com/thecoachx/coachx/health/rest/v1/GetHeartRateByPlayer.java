package com.thecoachx.coachx.health.rest.v1;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="A simple REST request to request for the heart rate of a person for a given session ID",
	exampleResponse="{\n  applicationStatus=\"OK\"\n}",
	exclude=true
)
public class GetHeartRateByPlayer implements RestGetEndpoint {

	
	@Override
	public String getPath() {
		return "/api/v1/heartratebyplayer/:personid/:sessionid";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			GetHeartRateByPlayerResponse response = new GetHeartRateByPlayerResponse();
			response.personId = Long.parseLong(req.params(":personid"));
			response.sessionId = Long.parseLong(req.params(":sessionid"));
			
			for (int i = 0; i < 200; i++) {
				HeartRateTuple heartRateTuple = new HeartRateTuple();
				heartRateTuple.heartRate = new Random().nextInt(200);
				heartRateTuple.timeStamp = System.currentTimeMillis();
				response.heartRate.add(heartRateTuple);
			}
			
			return new Gson().toJson(response);
		};
	}
	
	
	public static class GetHeartRateByPlayerResponse {
		public long personId;
		public long sessionId;
		public List<HeartRateTuple> heartRate = Lists.newLinkedList();
	}
	
	
	public static class HeartRateTuple {
		public long timeStamp;
		public int heartRate;
	}
}