package com.thecoachx.coachx.team.rest.v1;

import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.PersonAccess;
import com.thecoachx.coachx.core.persistence.access.TeamAccess;
import com.thecoachx.coachx.core.persistence.schema.Team;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.Error;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="Provides basic information about the team that is relevant to the current HTTP session",
	exampleResponse="{\n"
			      + "  teamId: 1,\n"
			      + "  teamName: \"Diamond Dogs\",\n"
			      + "  players: [\n"
			      + "    {\n"
			      + "      personId: 2,\n"
			      + "      firstName: \"Venom\",\n"
			      + "      lastName: \"Snake\",\n"
			      + "      image: \"http://diamond-dogs.org/venom.png\",\n"
			      + "      number: 32\n"
			      + "    },\n"
			      + "    ...\n"
			      + "  ]\n"
			      + "}"
)
public class GetTeam implements RestGetEndpoint {
	
	private TeamAccess teamAccess;
	private PersonAccess personAccess;

	@Inject
	GetTeam(TeamAccess teamAccess, PersonAccess personAccess) {
		this.teamAccess = teamAccess;
		this.personAccess = personAccess;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/team/:teamId";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			long teamId;
			
			try {
				teamId = Long.parseLong(req.params(":teamId"));
			} catch (Exception e) {
				return "Invalid team ID";
			}
			
			Optional<Team> teamByID = teamAccess.findById(teamId);
			
			if (teamByID.isPresent()) {
				Gson gson = new Gson();
				JsonObject teamJsonObject = gson.toJsonTree(teamByID.get()).getAsJsonObject();
				teamJsonObject.remove("id");
				teamJsonObject.remove("players");
				teamJsonObject.remove("coaches");
				teamJsonObject.add("players", new JsonArray());
				
				teamByID.get().getPlayers().forEach(id -> {
					JsonObject personJsonObject = gson.toJsonTree(personAccess.findById(id).get()).getAsJsonObject();
					personJsonObject.remove("id");
					teamJsonObject.get("players").getAsJsonArray().add(personJsonObject);
				});
				
				return gson.toJson(teamJsonObject);
			} else {
				return new Gson().toJson(Error.error("Invalid team ID", "No team found with ID: [" + teamId + "]"));
			}
		};
	}
}