package com.thecoachx.coachx.mvp;

import java.util.HashMap;
import java.util.TreeMap;

import org.mongodb.morphia.Datastore;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.thecoachx.coachx.core.StartupService;
import com.thecoachx.coachx.core.persistence.access.PersonAccess;
import com.thecoachx.coachx.core.persistence.access.SessionAccess;
import com.thecoachx.coachx.core.persistence.access.TeamAccess;
import com.thecoachx.coachx.core.persistence.schema.Person;
import com.thecoachx.coachx.core.persistence.schema.Session;
import com.thecoachx.coachx.core.persistence.schema.Team;
import com.thecoachx.coachx.core.sport.SportField;
import com.thecoachx.coachx.position.Position;
import com.thecoachx.coachx.position.SessionPositionHistoryCache;

/**
 * Provides a means to populate databases and caches for testing
 * 
 * @author Matt
 */
public class ProvideTestDataStartupService implements StartupService {

	private SessionPositionHistoryCache sessionPositionHistoryCache;
	private PersonAccess personAccess;
	private SessionAccess sessionAccess;
	private Datastore datastore;
	private TeamAccess teamAccess;

	@Inject
	ProvideTestDataStartupService(
		SessionPositionHistoryCache sessionPositionHistoryCache,
		PersonAccess personAccess,
		SessionAccess sessionAccess,
		TeamAccess teamAccess,
		Datastore datastore
	) {
		this.sessionPositionHistoryCache = sessionPositionHistoryCache;
		this.personAccess = personAccess;
		this.sessionAccess = sessionAccess;
		this.teamAccess = teamAccess;
		this.datastore = datastore;
	}

	
	@Override
	public void execute() {
		dropAllCollections();
		populatePositionCache();
		populateTeams();
		populatePlayers();
		populateSessions();
	}


	private void populateTeams() {
		Team team = new Team();
		team.setTeamId(1L);
		team.setTeamName("Team CX");
		team.getPlayers().add(1l);
		team.getPlayers().add(2l);
		team.getPlayers().add(3l);
		teamAccess.insert(team);
	}


	private void dropAllCollections() {
		datastore.getDB().getCollectionNames().forEach(collectionName -> {
			if (collectionName.startsWith("system")) {
				return;
			}
			
			datastore.getDB().getCollection(collectionName).remove(new BasicDBObject());
		});
	}


	private void populateSessions() {
		Session session = new Session();
		session.setLocation("Sunset Valley Highshool");
		session.setPersons(Lists.newArrayList(1L, 2L, 3L));
		session.setSessionId(1L);
		session.setSportField(SportField.FOOTBALL);
		session.setStartTime(1478736000000L);
		
		sessionAccess.insert(session);
	}


	private void populatePlayers() {
		Person person1 = new Person();
		person1.setFirstName("Noah");
		person1.setLastName("Orstein");
		person1.setPersonId(1);
		person1.setImage("http://icons.veryicon.com/256/System/Kameleon/Boss%203.png");
		person1.setNumber(66);
		
		Person person2 = new Person();
		person2.setFirstName("Michael");
		person2.setLastName("Ojunga");
		person2.setPersonId(2);
		person2.setImage("http://icons.veryicon.com/256/System/Kameleon/Boss%203.png");
		person2.setNumber(33);
		
		Person person3 = new Person();
		person3.setFirstName("Matt");
		person3.setLastName("Peck");
		person3.setPersonId(3);
		person3.setImage("http://icons.veryicon.com/256/System/Kameleon/Boss%203.png");
		person3.setNumber(88);
		
		personAccess.insert(person1);
		personAccess.insert(person2);
		personAccess.insert(person3);
	}


	private void populatePositionCache() {
		HashMap<Long, TreeMap<Long, Position>> playerToPositionMap = new HashMap<>();
		
		playerToPositionMap.put(1L, new TreeMap<>());
		playerToPositionMap.put(2L, new TreeMap<>());
		playerToPositionMap.put(3L, new TreeMap<>());
		
		for (int playerId = 1; playerId < 4; playerId++) {
			for (int i = 0; i < 10000; i++) {
				Position position = new Position();
				position.timeStamp = i * 125;
				position.x = i * 0.1d + playerId * 3;
				position.y = i * 0.03d;
				playerToPositionMap.get((long) playerId).put((long) i * 125, position);
			}
		}
		
		sessionPositionHistoryCache.put(1L, playerToPositionMap);
	}
}