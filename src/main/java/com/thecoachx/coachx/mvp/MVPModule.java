package com.thecoachx.coachx.mvp;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.multibindings.Multibinder;
import com.thecoachx.coachx.core.StartupService;
import com.thecoachx.coachx.core.rest.RestEndpoint;
import com.thecoachx.coachx.mvp.rest.v1.SeedDatabase;

/**
 * Module that populates database with dummy data, also populates caches with empty data
 * 
 * @author Matt
 */
public class MVPModule implements Module {

	
	@Override
	public void configure(Binder binder) {
		Multibinder<StartupService> startupServices = Multibinder.newSetBinder(binder, StartupService.class);
		startupServices.addBinding().to(ProvideTestDataStartupService.class);
		
		Multibinder<RestEndpoint> restEndpoints = Multibinder.newSetBinder(binder, RestEndpoint.class);
		restEndpoints.addBinding().to(SeedDatabase.class);
	}
}