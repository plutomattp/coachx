package com.thecoachx.coachx.mvp.rest.v1;

import com.google.inject.Inject;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.mvp.ProvideTestDataStartupService;

import spark.Route;

@Documentation(
	description="",
	exampleResponse="",
	exclude=true
)
public class SeedDatabase implements RestGetEndpoint {
	
	private ProvideTestDataStartupService provideTestDataStartupService;

	@Inject
	SeedDatabase(ProvideTestDataStartupService provideTestDataStartupService) {
		this.provideTestDataStartupService = provideTestDataStartupService;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/mvp/seedDatabase";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			provideTestDataStartupService.execute();
			return "Done";
		};
	}
}