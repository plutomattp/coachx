package com.thecoachx.coachx.app.rest.v1;

import java.math.BigInteger;
import java.security.SecureRandom;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.Error;
import com.thecoachx.coachx.core.rest.RestPostEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="Authenticates a user in the application, return an authentication token.",
	
	exampleBody="{\n"
			  + "  email: \"ocelot@outerheaven.com\",\n"
			  + "  password: \"4d4msk4\"\n"
			  + "}",
			  
	exampleResponse="{\n"
			      + "  teamId: 1,\n"
			      + "  isCoach: true,\n"
			      + "  authToken: \"rfr0qacd8cvdma5tts8p7c9s26\"\n"
			      + "}"
)
public class PostLogIn implements RestPostEndpoint {

	@Override
	public String getPath() {
		return "/api/v1/login";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			String authTokenAttribute = "authtoken";
			String teamAttribute = "teamId";
			long teamId = 1L;
			
			String authToken = req.session().attribute(authTokenAttribute);
			
			if (authToken == null) {
				JsonObject fromJson = new Gson().fromJson(req.body(), JsonObject.class);
				if (fromJson.has("email") && fromJson.has("password")) {
					
					SecureRandom random = new SecureRandom();
					String newAuthToken = new BigInteger(130, random).toString(32);
					req.session().attribute(authTokenAttribute, newAuthToken);
					
					return new Gson().toJson(new LogInResponse(teamId, true, newAuthToken));
				} else {
					return new Gson().toJson(Error.error("Invalid data format", "Incorrect data format, expected JSON object with 'email' and 'password' attributes"));
				}
			} else {
				return new Gson().toJson(
					new LogInResponse(
						req.session().attribute(teamAttribute), 
						true, 
						req.session().attribute(authTokenAttribute)
					)
				);
			}
		};
	}
	
	
	public static class LogInResponse {
		public final long teamId;
		public final boolean isCoach;
		public final String authToken;
		
		public LogInResponse(long teamId, boolean isCoach, String authToken) {
			this.teamId = teamId;
			this.isCoach = isCoach;
			this.authToken = authToken;
		}
	}
}