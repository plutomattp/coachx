package com.thecoachx.coachx.person.rest.v1;

import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.PersonAccess;
import com.thecoachx.coachx.core.persistence.schema.Person;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestPostEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */ 
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="A simple POST request to create a new person",
	exampleResponse="{\n  applicationStatus=\"OK\"\n}",
	exclude=true
)
public class NewPerson implements RestPostEndpoint {
	
	private PersonAccess playerAccess;

	@Inject
	NewPerson(PersonAccess playerAccess) {
		this.playerAccess = playerAccess;
	}
	

	@Override
	public String getPath() {
		return "/api/v1/newPerson";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			JsonObject fromJson = new Gson().fromJson(req.body(), JsonObject.class);
			
			Person newPerson = new Person();
			newPerson.setFirstName(fromJson.get("firstName").getAsString());
			newPerson.setLastName(fromJson.get("lastName").getAsString());
			newPerson.setPersonId(new Random().nextLong());
			
			playerAccess.insert(newPerson);
			
			NewPersonResponse newPersonResponse = new NewPersonResponse();
			newPersonResponse.firstName = newPerson.getFirstName();
			newPersonResponse.lastName = newPerson.getLastName();
			newPersonResponse.id = newPerson.getPersonId();
			
			return new Gson().toJson(newPersonResponse);
		};
	}
	
	
	public static class NewPersonResponse {
		public String firstName;
		public String lastName;
		public long id;
	}
}