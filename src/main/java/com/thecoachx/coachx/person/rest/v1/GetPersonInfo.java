package com.thecoachx.coachx.person.rest.v1;

import java.util.Optional;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.PersonAccess;
import com.thecoachx.coachx.core.persistence.schema.Person;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.Error;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */ 
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="Provides basic information regarding a person",
	exampleResponse="{\n"
			      + "  id: 3,\n"
			      + "  firstName: \"Solid\",\n"
			      + "  lastName: \"Snake\",\n"
			      + "  image: \"http://someurl.com/snake.png\",\n"
			      + "  number: 88\n"
			      + "}"
)
public class GetPersonInfo implements RestGetEndpoint {
	private PersonAccess personAccess;

	@Inject
	GetPersonInfo(PersonAccess personAccess) {
		this.personAccess = personAccess;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/personInfo/:personid";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			long personid;
			
			try {
				personid = Long.parseLong(req.params(":personid"));
			} catch (Exception e) {
				return "Invalid person ID";
			}
			
			Optional<Person> person = personAccess.findById(personid);
			
			if (person.isPresent()) {
				PersonInfoResponse response = new PersonInfoResponse();
				response.firstName = person.get().getFirstName();
				response.lastName = person.get().getLastName();
				response.id = person.get().getPersonId();
				response.image = person.get().getImage();
				response.number = person.get().getNumber();
				return new Gson().toJson(response);
			} else {
				return new Gson().toJson(Error.error("Not found", "No person found with id " + personid));
			}
		};
	}
	
	
	public static class PersonInfoResponse {
		public long id;
		public String firstName;
		public String lastName;
		public String image;
		public int number;
	}
}