package com.thecoachx.coachx.udp;

/**
 * Configurations for the data server
 * 
 * @author Matt
 */
public class UdpServerConfiguration {

	private int udpPort;
	
	/**
	 * @return the UDP port used for the data server
	 */
	public int getUDPPort() {
		return udpPort;
	}
}