package com.thecoachx.coachx.udp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * {@link UdpMessageHandler} that prints the received UDP data to the console.
 * 
 * @author Matt
 */
public class PrintToConsoleMessageHandler implements UdpMessageHandler {

	@Override
	public void handle(byte[] bytes, int length) {
		
		int l = 0;
		
		byte[] x = new byte[4];
		byte[] y = new byte[4];
		byte[] z = new byte[4];
		
		StringBuilder builder = new StringBuilder();
		for (byte b : bytes) {
			l++;
			builder.append(String.format("%02x", b));
			
			if (l >= 28 && l <= 31) {
				x[l - 28] = b;
			}
			
			if (l >= 32 && l <= 35) {
				y[l - 32] = b;
			}
			
			if (l >= 36 && l <= 39) {
				z[l - 36] = b;
			}
			
			if (l >= length) {
				break;
			}
		}
		
		String string = builder.toString();
		
		if (string.substring(12, 14).equals("04")) {
			String payload = string.substring(16);
			
			System.out.print("TimeStamp: " + payload.substring(0, 16));
			System.out.print(" Device type: " + (payload.substring(16, 18).equals("01") ? "Tag" : "Anchor"));
			System.out.print(" Device address: " + payload.substring(18, 26));
			System.out.print(" Device status: " + (payload.substring(26, 28).equals("00") ? "OK" : "??"));
			System.out.print(" PositionX: " + ByteBuffer.wrap(x).order(ByteOrder.BIG_ENDIAN).getFloat());
			System.out.print(" PositionY: " + ByteBuffer.wrap(y).order(ByteOrder.BIG_ENDIAN).getFloat());
			System.out.println(" PositionZ: " + ByteBuffer.wrap(z).order(ByteOrder.BIG_ENDIAN).getFloat());
			
			
		}
	}
}