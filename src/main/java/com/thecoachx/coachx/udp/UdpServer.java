package com.thecoachx.coachx.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import com.google.inject.Inject;

/**
 * Service to listen to packets sent via UDP or TCP
 * 
 * @author Matt
 */
public class UdpServer {
	private UdpServerConfiguration dataServerConfiguration;
	private UdpMessageHandler udpMessageHandler;

	@Inject
	UdpServer(UdpServerConfiguration dataServerConfiguration, UdpMessageHandler udpMessageHandler) {
		this.dataServerConfiguration = dataServerConfiguration;
		this.udpMessageHandler = udpMessageHandler;
	}
	
	
	/**
	 * Starts this data server
	 * 
	 * @param tcpPort
	 * @param udpPort
	 */
	public void start() {
		try {
			DatagramSocket socket = new DatagramSocket(dataServerConfiguration.getUDPPort());
			DatagramPacket datagramPacket = new DatagramPacket(new byte[1024], 1024);
			
			Thread t = new Thread(() -> {
				try {
					while (true) {
						socket.receive(datagramPacket);
						udpMessageHandler.handle(datagramPacket.getData(), datagramPacket.getLength());
					}
				} catch (Exception e) {
					socket.close();
					throw new RuntimeException(e);
				}
			});
			
			t.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}