package com.thecoachx.coachx.udp;

import com.google.inject.ImplementedBy;

/**
 * Interface for handling incoming UDP messages
 * 
 * @author Matt
 */
@ImplementedBy(PrintToConsoleMessageHandler.class)
public interface UdpMessageHandler {

	/**
	 * Handles the incoming UDP message
	 * @param bs 
	 */
	public void handle(byte[] bytes, int length);
}