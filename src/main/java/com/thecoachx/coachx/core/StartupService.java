package com.thecoachx.coachx.core;

/**
 * Any service that should be run as part of startup
 * 
 * @author Matt
 */
public interface StartupService {
	public void execute();
}