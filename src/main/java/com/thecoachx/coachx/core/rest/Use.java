package com.thecoachx.coachx.core.rest;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Represents the middleware to use for the rest endpoint, order matters here, the lower index routes are used first
 * 
 * @author Matt
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Use {
	public Class<? extends Middleware>[] value();
}