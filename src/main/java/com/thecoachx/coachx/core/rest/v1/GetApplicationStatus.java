package com.thecoachx.coachx.core.rest.v1;

import com.google.gson.Gson;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */ 
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="Indicates the current status of the application.",
	exampleResponse="{\n  applicationStatus: \"OK\"\n}"
)
public class GetApplicationStatus implements RestGetEndpoint {
	
	
	@Override
	public String getPath() {
		return "/api/v1/applicationStatus";
	}
	

	@Override
	public Route getRoute() {
		return (req, res) -> {
			return new Gson().toJson(new ApplicationStatusResponse());
		};
	}
	
	
	public static class ApplicationStatusResponse {
		public String applicationStatus = "OK";
	}
}