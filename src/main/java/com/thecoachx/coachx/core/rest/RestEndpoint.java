package com.thecoachx.coachx.core.rest;

import com.google.inject.Injector;

/**
 * RESTful endpoint
 * 
 * @author Matt
 */
public interface RestEndpoint {
	
	/**
	 * @return String representation of HTTP method
	 */
	public String getHttpMethod();
	
	/**
	 * @return the Path of this GET endpoint
	 */
	public String getPath();

	/**
	 * Registers the REST endpoint 
	 */
	public void register(Injector injector);
}