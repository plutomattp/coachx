package com.thecoachx.coachx.core.rest;

import spark.Request;
import spark.Response;

/**
 * Middleware used to handle a REST api call
 * 
 * @author Matt
 */
public interface Middleware {

	/**
	 * Handles the request/response
	 * 
	 * @param req
	 * @param res
	 */
	public void handle(Request req, Response res);
}