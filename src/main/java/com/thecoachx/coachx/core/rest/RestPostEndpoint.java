package com.thecoachx.coachx.core.rest;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.inject.Injector;

import spark.Route;
import spark.Spark;

/**
 * A HTTP POST request
 * 
 * @author Matt
 */
public interface RestPostEndpoint extends RestEndpoint {

	@Override
	public default void register(Injector injector) {
		List<Middleware> middlewares = Lists.newLinkedList();
		
		if (getClass().isAnnotationPresent(Use.class)) {
			for (Class<? extends Middleware> middleware : getClass().getAnnotation(Use.class).value()) {
				middlewares.add(injector.getInstance(middleware));
			}
		}
		
		Spark.post(getPath(), (req, res) -> {
			middlewares.stream().forEach(mw -> {
				mw.handle(req, res);
			});
			
			return getRoute().handle(req, res);
		});
	}
	
	
	@Override
	public default String getHttpMethod() {
		return "POST";
	}
	
	
	/**
	 * @return the {@link Route} of this POST endpoint
	 */
	public Route getRoute();
}