package com.thecoachx.coachx.core.rest;

/**
 * Represents an error
 * 
 * @author Matt
 */
public class Error {
	public final String error;
	public final String explanation;

	private Error(String error, String explanation) {
		this.error = error;
		this.explanation = explanation;
	}
	
	public static Error error(String error, String explanation) {
		return new Error(error, explanation);
	}
}