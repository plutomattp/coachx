package com.thecoachx.coachx.core.rest;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used for generating documentation for exposed REST web services
 * 
 * @author Matt
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Documentation {

	/**
	 * @return The description of the endpoint
	 */
	public String description();
	
	/**
	 * @return An example body of the request
	 */
	public String exampleBody() default "N/A";
	
	/**
	 * @return An example response of this API
	 */
	public String exampleResponse();
	
	/**
	 * @return Whether this API should be excluded from documentation
	 */
	public boolean exclude() default false;
}