package com.thecoachx.coachx.core.rest;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.inject.Injector;

import spark.Route;
import spark.Spark;

/**
 * A HTTP GET endpoint as a {@link RestEndpoint}
 * 
 * @author Matt
 */
public interface RestGetEndpoint extends RestEndpoint { 
	
	@Override
	public default void register(Injector injector) {
		List<Middleware> middlewares = Lists.newLinkedList();
		
		if (getClass().isAnnotationPresent(Use.class)) {
			for (Class<? extends Middleware> middleware : getClass().getAnnotation(Use.class).value()) {
				middlewares.add(injector.getInstance(middleware));
			}
		}
		
		Spark.get(getPath(), (req, res) -> {
			middlewares.stream().forEach(mw -> {
				mw.handle(req, res);
			});
			
			res.type("application/json");
			
			return getRoute().handle(req, res);
		});
	}
	
	
	@Override
	public default String getHttpMethod() {
		return "GET";
	}
	
	
	/**
	 * @return the {@link Route} of this GET endpoint
	 */
	public Route getRoute();
}