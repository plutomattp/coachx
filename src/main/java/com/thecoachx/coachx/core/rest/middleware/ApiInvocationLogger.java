package com.thecoachx.coachx.core.rest.middleware;

import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.ApiInvocationAccess;
import com.thecoachx.coachx.core.persistence.schema.ApiInvocation;
import com.thecoachx.coachx.core.rest.Middleware;

import spark.Request;
import spark.Response;

/**
 * A simple middleware that logs API calls to the database
 * 
 * @author Matt
 */
public class ApiInvocationLogger implements Middleware {
	
	private ApiInvocationAccess apiInvocationAccess;

	@Inject
	ApiInvocationLogger(ApiInvocationAccess apiInvocationAccess) {
		this.apiInvocationAccess = apiInvocationAccess;
	}
	

	@Override
	public void handle(Request req, Response res) {
		ApiInvocation entity = new ApiInvocation();
		entity.setApi(req.pathInfo());
		entity.setTimestamp(System.currentTimeMillis());
		entity.setIp(req.ip());
		apiInvocationAccess.insert(entity);
	}
}