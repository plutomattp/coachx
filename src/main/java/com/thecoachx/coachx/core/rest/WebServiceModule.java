package com.thecoachx.coachx.core.rest;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.multibindings.Multibinder;
import com.thecoachx.coachx.app.rest.v1.PostLogIn;
import com.thecoachx.coachx.chat.rest.v1.GetConversation;
import com.thecoachx.coachx.chat.rest.v1.NewChatMessage;
import com.thecoachx.coachx.chat.rest.v1.NewConversation;
import com.thecoachx.coachx.core.rest.v1.GetApplicationStatus;
import com.thecoachx.coachx.health.rest.v1.GetHeartRateByPlayer;
import com.thecoachx.coachx.person.rest.v1.GetPersonInfo;
import com.thecoachx.coachx.person.rest.v1.NewPerson;
import com.thecoachx.coachx.position.rest.v1.GetPositionHistoryBySession;
import com.thecoachx.coachx.session.rest.v1.GetSession;
import com.thecoachx.coachx.team.rest.v1.GetTeam;

/**
 * Web service module
 * 
 * @author Matt
 */
public class WebServiceModule implements Module {

	@Override
	public void configure(Binder binder) {
		bindRestEndpoints(binder);
	}
	
	
	private void bindRestEndpoints(Binder binder) {
		Multibinder<RestEndpoint> restEndpoints = Multibinder.newSetBinder(binder, RestEndpoint.class);
		exposed(restEndpoints);
	}

	
	private void exposed(Multibinder<RestEndpoint> restEndpoints) {
		restEndpoints.addBinding().to(GetApplicationStatus.class);
		restEndpoints.addBinding().to(GetPositionHistoryBySession.class);
		restEndpoints.addBinding().to(GetPersonInfo.class);
		restEndpoints.addBinding().to(PostLogIn.class);
		restEndpoints.addBinding().to(GetSession.class);
		restEndpoints.addBinding().to(GetTeam.class);
		
		restEndpoints.addBinding().to(NewPerson.class);
		restEndpoints.addBinding().to(GetConversation.class);
		restEndpoints.addBinding().to(NewChatMessage.class);
		restEndpoints.addBinding().to(NewConversation.class);
		restEndpoints.addBinding().to(GetHeartRateByPlayer.class);
	}
}
