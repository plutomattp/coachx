package com.thecoachx.coachx.core;

import static spark.SparkBase.stop;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.inject.Inject;

/**
 * The configuration parser reads the application configuration file
 * 
 * @author Matt
 */
public class ConfigurationParser {
	
	private String configPath;
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationParser.class);
	
	@Inject
	ConfigurationParser(@ConfigurationPath String configPath) {
		this.configPath = configPath;
	}
	
	
	/**
	 * Parses the configuration file into a configuration class
	 */
	public <T> T read(Class<T> toRead, String overrideName) {
		try {
			JsonObject configurationInJsonForm = new JsonParser().parse(new JsonReader(new FileReader(configPath))).getAsJsonObject();
			return new Gson().fromJson(configurationInJsonForm.get(overrideName), toRead);
			
		} catch (JsonIOException e) {
			LOGGER.error("Error when parsing configuration file", e);
		} catch (JsonSyntaxException e) { 
			LOGGER.error("Error when parsing configuration file", e);
		} catch (FileNotFoundException e) {
			LOGGER.error("Configuration file not found under path: [" + configPath + "]", e);
		}
		
		// Stop the server
		stop();
		
		// To satisfy the compiler
		throw new RuntimeException();
	}
	
	
	/**
	 * Parses the configuration file into a configuration class
	 */
	public <T> T read(Class<T> toRead) {
		return read(toRead, toRead.getSimpleName());
	}
}