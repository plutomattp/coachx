package com.thecoachx.coachx.core.sport;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Represents a sports field, top left is always assumed to be 0, 0 on the x-y plane, with y-axis positive going down, and x-axis positive going right
 * 
 * @author Matt
 */
public enum SportField {
	FOOTBALL(FieldType.FOOTBALL, 109.728D, 48.768D),
	SOCCER(FieldType.SOCCER, 105D, 68D),
	LACROSSE(FieldType.LACROSSE, 100.584D, 54.864D);
	
	private final FieldType fieldType;
	private final double length;
	private final double height;
	
	private SportField(FieldType type, double length, double height) {
		this.fieldType = type;
		this.length = length;
		this.height = height;
	}

	public double getHeight() {
		return height;
	}
	
	public double getLength() {
		return length;
	}

	public FieldType getFieldType() {
		return fieldType;
	}
	
	public JsonObject getJsonRepresentation() {
		JsonObject o = new JsonObject();
		
		o.add("fieldType", new JsonPrimitive(fieldType.toString()));
		o.add("length", new JsonPrimitive(length));
		o.add("height", new JsonPrimitive(height));
		
		return o;
	}
	
	public enum FieldType {
		FOOTBALL,
		SOCCER,
		LACROSSE
	}
}