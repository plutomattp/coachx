package com.thecoachx.coachx.core;

import static spark.Spark.halt;
import static spark.SparkBase.port;

import java.util.Set;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.thecoachx.coachx.core.rest.Error;
import com.thecoachx.coachx.core.rest.RestEndpoint;
import com.thecoachx.coachx.frontend.FrontendRegistrationService;
import com.thecoachx.coachx.frontend.GetApiDocumentation;
import com.thecoachx.coachx.udp.UdpServer;

import spark.Spark;

/**
 * Main application class
 * 
 * @author Matt
 */
class Application {
	
	private Provider<Set<RestEndpoint>> endpoints;
	private Provider<UdpServer> dataServer;
	private Provider<FrontendRegistrationService> frontendRegistrationService;
	private Provider<Injector> injector;
	private Provider<Set<StartupService>> startupServices;

	/**
	 * DI hookpoint
	 */
	@Inject
	Application(
		Provider<Set<RestEndpoint>> endpoints, 
		Provider<UdpServer> dataServer,
		Provider<FrontendRegistrationService> frontendRegistrationService,
		Provider<Injector> injector,
		Provider<Set<StartupService>> startupServices
	) {
		this.endpoints = endpoints;
		this.dataServer = dataServer;
		this.frontendRegistrationService = frontendRegistrationService;
		this.injector = injector;
		this.startupServices = startupServices;
	}
	
	
	/**
	 * Start the application server
	 */
	public void start() {
		configureEmbeddedWebServer();
		
		registerFrontend();
		registerRestEndpoints();
		startDataServer();
		runStartupServices();
	}


	/**
	 * Execute all startup services
	 */
	private void runStartupServices() {
		startupServices.get().forEach(service -> {
			service.execute();
		});
	}


	/**
	 * Start the data server
	 */
	private void startDataServer() {
		dataServer.get().start();
	}


	/**
	 * Configure properties for the Spark embedded web server
	 */
	private void configureEmbeddedWebServer() {
	    String envPort = System.getenv("PORT");
	    if (envPort == null) {
	    	port(2550);
	    } else {
	    	port(Integer.valueOf(envPort));
	    }
	}


	/**
	 * Registers all REST endpoints
	 */
	private void registerRestEndpoints() {
		for (RestEndpoint endpoint : endpoints.get()) {
			endpoint.register(injector.get());
		}
		
		injector.get().getInstance(GetApiDocumentation.class).register(injector.get());
		
		Spark.before((req, res) -> {
			if (!req.pathInfo().startsWith("/api/v1")) {
				return;
			}
			
			if (req.pathInfo().startsWith("/api/v1/login")) {
				return;
			}
			
			String authToken = req.session().attribute("authtoken");
			
			if (authToken == null) {
				halt(401, new Gson().toJson(Error.error("unauthorized", "please log in to continue")));
			}
		});
	}


	private void registerFrontend() {
		frontendRegistrationService.get().register();
	}
}
