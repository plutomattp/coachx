package com.thecoachx.coachx.core;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.google.inject.BindingAnnotation;

/**
 * Annotation for DI
 * 
 * @author Matt
 */
@Retention(RetentionPolicy.RUNTIME)
@BindingAnnotation
@interface ConfigurationPath {
}