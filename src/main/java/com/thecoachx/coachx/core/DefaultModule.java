package com.thecoachx.coachx.core;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.thecoachx.coachx.core.persistence.DatabaseModule;
import com.thecoachx.coachx.core.rest.WebServiceModule;
import com.thecoachx.coachx.frontend.FrontendConfiguration;
import com.thecoachx.coachx.mvp.MVPModule;
import com.thecoachx.coachx.position.PositionHistoryModule;
import com.thecoachx.coachx.udp.UdpServerConfiguration;

/**
 * Default module for Guice DI
 * 
 * @author Matt
 */
class DefaultModule implements Module {
	private static final String CONFIG_PATH = "src/main/resources/spark/config.json";
	
	@Override
	public void configure(Binder binder) {
		binder.install(new DatabaseModule());
		binder.install(new WebServiceModule());
		binder.install(new PositionHistoryModule());
		binder.install(new MVPModule());
		
		bindConfigPath(binder);
	}


	private void bindConfigPath(Binder binder) {
		binder.bind(String.class).annotatedWith(ConfigurationPath.class).toInstance(CONFIG_PATH);
	}


	@Provides
	@Singleton
	public FrontendConfiguration provideEmbeddedServerConfiguration(ConfigurationParser configurationParser) {
		return configurationParser.read(FrontendConfiguration.class);
	}
	
	
	@Provides
	@Singleton
	public UdpServerConfiguration provideUdpServerConfiguration(ConfigurationParser configurationParser) {
		return configurationParser.read(UdpServerConfiguration.class);
	}
}