package com.thecoachx.coachx.core;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

/**
 * Main application entry point
 */
public class Main {
	
    public static void main(String[] args) {
    	Injector injector = Guice.createInjector(loadGuiceModules());
    	injector.getInstance(Application.class).start();
    }

    
	private static Module loadGuiceModules() {
		return new DefaultModule();
	}
}
