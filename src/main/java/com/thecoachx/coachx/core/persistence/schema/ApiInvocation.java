package com.thecoachx.coachx.core.persistence.schema;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * A persistent entity that stores information of a specific REST API invocation
 * 
 * @author Matt
 */
@Entity("ApiInvocation")
public class ApiInvocation {

    @Id
    private ObjectId id;
	private long timestamp;
	private String ip;
	private String api;
	
	/**
	 * @return the API endpoint that was invoked
	 */
	public String getApi() {
		return api;
	}
	
	
	public void setApi(String api) {
		this.api = api;
	}
	
	
	/**
	 * @return the IP address of the requestor
	 */
	public String getIp() {
		return ip;
	}
	
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	
	/**
	 * @return the timestamp of the API invocation, measured in UNIX time
	 */
	public long getTimestamp() {
		return timestamp;
	}
	
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}