package com.thecoachx.coachx.core.persistence;

import org.mongodb.morphia.query.UpdateOperations;

import com.google.inject.Inject;

/**
 * Database access class at entity level
 * 
 * @author Matt
 */
public abstract class DatabaseAccess<T> {

	protected DatabaseFacade db;
	protected Class<T> entityClass;

	@Inject
	public DatabaseAccess(DatabaseFacade db, Class<T> entityClass) {
		this.db = db;
		this.entityClass = entityClass;
	}
	
	
	/**
	 * @param entity to insert
	 */
	public void insert(T entity) {
		db.insert(entity);
	}
	
	
	/**
	 * @param entity to update
	 * @return an {@link UpdateOperations}
	 */
	public UpdateOperations<T> update(T entity) {
		return db.update(entity);
	}
}