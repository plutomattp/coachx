package com.thecoachx.coachx.core.persistence;

import java.util.Set;

import com.google.common.collect.Sets;
import com.google.inject.Singleton;
import com.thecoachx.coachx.core.persistence.schema.ApiInvocation;
import com.thecoachx.coachx.core.persistence.schema.Person;
import com.thecoachx.coachx.core.persistence.schema.Session;

/**
 * Holds all entities that are in use
 * 
 * @author Matt
 */
@Singleton
public class Schema {

	/**
	 * All entities in use
	 */
	public final Set<Class<?>> entities = Sets.newHashSet(
		ApiInvocation.class,
		Person.class,
		Session.class
	);
}