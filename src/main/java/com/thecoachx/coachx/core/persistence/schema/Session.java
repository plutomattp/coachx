package com.thecoachx.coachx.core.persistence.schema;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

import com.thecoachx.coachx.core.sport.SportField;

/**
 * A session is a sport session/game, i.e. a soccer game half
 * 
 * @author Matt
 */
@Indexes({
	@Index(
		fields = {
			@Field("sessionId")
		}
	),
	@Index(
		fields = {
			@Field("persons")
		}
	),
	@Index(
		fields = {
			@Field("sessionType")
		}
	)
})
public class Session {

	@Id
	private ObjectId id;
	private long sessionId;
	private long startTime;
	private String location;
	private SportField sportField;
	private List<Long> persons;
	
	
	public long getSessionId() {
		return sessionId;
	}
	
	
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	
	
	public String getLocation() {
		return location;
	}
	
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	public List<Long> getPersons() {
		return persons;
	}


	public void setPersons(List<Long> persons) {
		this.persons = persons;
	}


	public long getStartTime() {
		return startTime;
	}


	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}


	public SportField getSportField() {
		return sportField;
	}


	public void setSportField(SportField sportField) {
		this.sportField = sportField;
	}
}