package com.thecoachx.coachx.core.persistence;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import com.google.inject.Inject;
import com.google.inject.Singleton;
 
/**
 * Emulates database operations
 * 
 * @author Matt
 */
@Singleton
public class DatabaseFacade {

	private Datastore datastore;

	@Inject
	DatabaseFacade(Datastore datastore) {
		this.datastore = datastore;
	}
	
	
	/**
	 * @param entity to insert into the database
	 */
	public <T> void insert(T entity) {
		datastore.save(entity);
	}
	
	
	/**
	 * @param entity to update
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	public <T> UpdateOperations<T> update(T entity) {
		return datastore.createUpdateOperations((Class<T>) entity.getClass());
	}
	
	
	/**
	 * @param clazz of the collection object
	 * 
	 * @return a query builder
	 */
	public <T> Query<T> createQuery(Class<T> clazz) {
		return datastore.createQuery(clazz);
	}
	
	
	/**
	 * @param entity to update
	 * @param ops to perform
	 */
	public <T> void performUpdate(T entity, UpdateOperations<T> ops) {
		datastore.update(entity, ops);
	}
}