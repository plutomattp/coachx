package com.thecoachx.coachx.core.persistence.access;

import java.util.List;
import java.util.Optional;

import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.DatabaseAccess;
import com.thecoachx.coachx.core.persistence.DatabaseFacade;
import com.thecoachx.coachx.core.persistence.schema.ChatMessage;
import com.thecoachx.coachx.core.persistence.schema.Conversation;

public class ConversationAccess extends DatabaseAccess<Conversation> {

	@Inject
	ConversationAccess(DatabaseFacade db) {
		super(db, Conversation.class);
	}
	
	
	/**
	 * @param conversationId of the conversation
	 * 
	 * @return a {@link Conversation} with matching ID
	 */
	public Optional<Conversation> findById(long conversationId) {
		Conversation convo = db.createQuery(Conversation.class).field("conversationId").equal(conversationId).get();
		if (convo == null) {
			return Optional.empty();
		}
		
		return Optional.of(convo);
	}
	
	
	/**
	 * @param participantId of the participant
	 * 
	 * @return a list of {@link Conversation} the participant has taken part in, ordered by time of the most recent message, descending
	 */
	public List<Conversation> listByParticipant(long participantId) {
		List<Conversation> asList = db.createQuery(Conversation.class).field("participantId").contains(Long.toString(participantId)).asList();
		asList.sort((c1, c2) -> Long.compare(c1.getMessages().get(0).getTimeStamp(), c2.getMessages().get(0).getTimeStamp()));
		return asList;
	}
	
	
	/**
	 * @param conversationId of the conversation
	 * @param toAdd message to add
	 */
	public void addMessage(long conversationId, ChatMessage toAdd) {
		Conversation entity = findById(conversationId).get();
		db.performUpdate(entity, update(entity).add("messages", toAdd));
	}
}
