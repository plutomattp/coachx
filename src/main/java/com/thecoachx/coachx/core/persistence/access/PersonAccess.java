package com.thecoachx.coachx.core.persistence.access;

import java.util.Optional;

import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.DatabaseAccess;
import com.thecoachx.coachx.core.persistence.DatabaseFacade;
import com.thecoachx.coachx.core.persistence.schema.Person;

/**
 * Access calss for {@link Person}
 * 
 * @author Matt
 */
public class PersonAccess extends DatabaseAccess<Person> {

	@Inject
	public PersonAccess(DatabaseFacade db) {
		super(db, Person.class);
	}

	
	/**
	 * @param playerid of the player
	 * 
	 * @return a {@link Person} with matching ID
	 */
	public Optional<Person> findById(long playerid) {
		Person player = db.createQuery(Person.class).field("personId").equal(playerid).get();
		if (player == null) {
			return Optional.empty();
		}
		
		return Optional.of(player);
	}
}
