package com.thecoachx.coachx.core.persistence.schema;

/**
 * Represents a chat message
 * 
 * @author Matt
 */
public class ChatMessage {

	/**
	 * Unique person ID of the message sender
	 */
	private long senderId;
	
	/**
	 * Timestamp of when the message was sent
	 */
	private long timeStamp;
	
	/**
	 * The content of the message
	 */
	private String message;

	
	public long getSenderId() {
		return senderId;
	}

	
	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}
	

	public long getTimeStamp() {
		return timeStamp;
	}

	
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	
	public String getMessage() {
		return message;
	}

	
	public void setMessage(String message) {
		this.message = message;
	}
}