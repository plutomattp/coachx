package com.thecoachx.coachx.core.persistence.schema;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexes;

import com.google.common.collect.Lists;

/**
 * Conversation between two people
 * 
 * @author Matt
 */
@Entity("Conversation")
@Indexes({
	@Index(
		fields = {
			@Field("participants")
		}
	),
	@Index(
		fields = {
			@Field("conversationId")
		},
		options = @IndexOptions(unique = true)
	)
})
public class Conversation {
	
	@Id
	private ObjectId id;
	
	
	/**
	 * Unique ID of the conversation
	 */
	private long conversationId;

	/**
	 * List of message in the conversation, with the first element in the list being the most recent message
	 */
	private final List<ChatMessage> messages = Lists.newLinkedList();
	
	/**
	 * Unique person IDs of all participants in this conversation
	 */
	private final List<Long> participants = Lists.newArrayList();
	
	
	public List<ChatMessage> getMessages() {
		return messages;
	}


	public List<Long> getParticipants() {
		return participants;
	}


	public long getConversationId() {
		return conversationId;
	}
	
	
	public void setConversationId(long conversationId) {
		this.conversationId = conversationId;
	}
}