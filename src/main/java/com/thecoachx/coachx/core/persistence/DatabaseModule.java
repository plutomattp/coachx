package com.thecoachx.coachx.core.persistence;

import static java.lang.System.getProperty;

import org.apache.commons.lang3.StringUtils;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.google.common.collect.Lists;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.thecoachx.coachx.core.ConfigurationParser;

/**
 * DI module for persistence
 * 
 * @author Matt
 */
public class DatabaseModule implements Module {

	@Override
	public void configure(Binder binder) {
	}


	@Provides
	@Singleton
	public DatabaseConfiguration provideDatabaseConfiguration(ConfigurationParser configurationParser) {
		if (StringUtils.isEmpty(getProperty("local"))) {
			return configurationParser.read(DatabaseConfiguration.class);
		} else {
			return configurationParser.read(DatabaseConfiguration.class, "DatabaseConfigurationLocal");
		}
	}
	
	
	@Provides
	@Singleton
	public MongoClient provideMongoClient(DatabaseConfiguration databaseConfiguration) {
		if (StringUtils.isEmpty(databaseConfiguration.getUsername())) {
			return new MongoClient(
				databaseConfiguration.getHost(),
				databaseConfiguration.getPort()
			);
		} else {
			return new MongoClient(
				new ServerAddress(databaseConfiguration.getHost(), databaseConfiguration.getPort()),
				Lists.newArrayList(MongoCredential.createCredential(
					databaseConfiguration.getUsername(), 
					databaseConfiguration.getDatabase(), 
					databaseConfiguration.getPassword().toCharArray()
				))
			);
		}
	}
	
	
	@Provides
	@Singleton
	public MongoDatabase provideMongoDatabase(MongoClient mongoClient, DatabaseConfiguration databaseConfiguration, Schema schema) {
		return mongoClient.getDatabase(databaseConfiguration.getDatabase());
	}
	
	
	@Provides
	@Singleton
	public Datastore provideMorphiaDatastore(MongoClient mongoClient, DatabaseConfiguration databaseConfiguration) {
		Morphia morphia = new Morphia();
		morphia.mapPackage("com.thecoachx.coachx.core.persistence.schema");
		
		final Datastore datastore = morphia.createDatastore(mongoClient, databaseConfiguration.getDatabase());
		datastore.ensureIndexes();
		
		return datastore;
	}
}