package com.thecoachx.coachx.core.persistence;

/**
 * Database configuration
 * 
 * @author Matt
 */
public class DatabaseConfiguration {

	private String host;
	private int port;
	private String database;
	private String username;
	private String password;
	
	/**
	 * @return the host of the database server
	 */
	public String getHost() {
		return host;
	}
	
	
	/**
	 * @return the port of the database server
	 */
	public int getPort() {
		return port;
	}
	
	
	/**
	 * @return the database name
	 */
	public String getDatabase() {
		return database;
	}
	
	
	/**
	 * @return the username to connect to the DB as
	 */
	public String getUsername() {
		return username;
	}
	
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
}