package com.thecoachx.coachx.core.persistence.access;

import java.util.Optional;

import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.DatabaseAccess;
import com.thecoachx.coachx.core.persistence.DatabaseFacade;
import com.thecoachx.coachx.core.persistence.schema.Team;

public class TeamAccess extends DatabaseAccess<Team> {

	@Inject
	TeamAccess(DatabaseFacade db) {
		super(db, Team.class);
	}
	
	
	/**
	 * @param teamId
	 * 
	 * @return a {@link Team} with matching ID
	 */
	public Optional<Team> findById(long teamId) {
		Team team = db.createQuery(Team.class).field("teamId").equal(teamId).get();
		if (team == null) {
			return Optional.empty();
		}
		
		return Optional.of(team);
	}
}