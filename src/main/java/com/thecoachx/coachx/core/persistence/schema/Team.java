package com.thecoachx.coachx.core.persistence.schema;

import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexes;

/**
 * Represents a team
 * 
 * @author Matt
 */
@Entity("Team")
@Indexes({
	@Index(
		fields = {
			@Field("teamId")
		},
		options = @IndexOptions(unique = true)
	),
	@Index(
		fields = {
			@Field("teamName")
		}
	),
	@Index(
		fields = {
			@Field("players")
		}
	),
	@Index(
		fields = {
			@Field("coaches")
		}
	)
})
public class Team {

	@Id
	private ObjectId id;
	private long teamId;
	private String teamName;
	private final List<Long> players = new LinkedList<>();
	private final List<Long> coaches = new LinkedList<>();
	
	
	public long getTeamId() {
		return teamId;
	}
	
	
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}


	public String getTeamName() {
		return teamName;
	}


	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}


	public List<Long> getPlayers() {
		return players;
	}


	public List<Long> getCoaches() {
		return coaches;
	}
}
