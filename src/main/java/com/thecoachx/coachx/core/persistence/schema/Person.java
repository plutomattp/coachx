package com.thecoachx.coachx.core.persistence.schema;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexes;

/**
 * Represents a person in the system
 * 
 * @author Matt
 */
@Entity("Person")
@Indexes({
	@Index(
		fields = {
			@Field("personId")
		},
		options = @IndexOptions(unique = true)
	),
	@Index(
		fields = {
			@Field("lastName")
		}
	)
})
public class Person {

	@Id
	private ObjectId id;
	private long personId;
	private String firstName;
	private String lastName;
	private String image;
	private int number;
	
	/**
	 * @return the unique person ID
	 */
	public long getPersonId() {
		return personId;
	}


	public void setPersonId(long personId) {
		this.personId = personId;
	}
	
	
	/**
	 * @return the first name of the person
	 */
	public String getFirstName() {
		return firstName;
	}
	
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the last name of the person
	 */
	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public int getNumber() {
		return number;
	}


	public void setNumber(int number) {
		this.number = number;
	}
}