package com.thecoachx.coachx.core.persistence.access;

import java.util.Optional;

import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.DatabaseAccess;
import com.thecoachx.coachx.core.persistence.DatabaseFacade;
import com.thecoachx.coachx.core.persistence.schema.Session;

public class SessionAccess extends DatabaseAccess<Session> {

	@Inject
	SessionAccess(DatabaseFacade db) {
		super(db, Session.class);
	}
	
	
	/**
	 * @param sessionId to search a session by
	 */
	public Optional<Session> getSessionById(long sessionId) {
		Session sesh = db.createQuery(Session.class).field("sessionId").equal(sessionId).get();
		if (sesh == null) {
			return Optional.empty();
		}
		
		return Optional.of(sesh);
	}
}
