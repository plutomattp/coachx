package com.thecoachx.coachx.core.persistence.access;

import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.DatabaseAccess;
import com.thecoachx.coachx.core.persistence.DatabaseFacade;
import com.thecoachx.coachx.core.persistence.schema.ApiInvocation;

/**
 * Accessor class for {@link ApiInvocation}
 * 
 * @author Matt
 */
public class ApiInvocationAccess extends DatabaseAccess<ApiInvocation> {

	@Inject
	ApiInvocationAccess(DatabaseFacade db) {
		super(db, ApiInvocation.class);
	}
}