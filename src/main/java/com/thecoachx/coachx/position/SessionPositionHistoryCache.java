package com.thecoachx.coachx.position;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * SIngleton cache for storing real time position data
 * 
 * @author Matt
 */
@Singleton
public class SessionPositionHistoryCache {
	
	private SessionPositionHistoryCacheConfiguration positionHistoryCacheConfiguration;
	
	/**
	 * This cache maps a session ID to a position history map
	 * 
	 * the position history map maps a person/player ID to another map which contains the actual position history of the player
	 */
	private Map<Long, Map<Long, TreeMap<Long, Position>>> cache = new HashMap<>();
	
	
	@Inject
	SessionPositionHistoryCache(SessionPositionHistoryCacheConfiguration positionHistoryCacheConfiguration) {
		this.positionHistoryCacheConfiguration = positionHistoryCacheConfiguration;
	}
	
	
	/**
	 * @param sessionId
	 * @param positionHistory
	 * 
	 * Adds an entry to the cache
	 */
	public void put(long sessionId, Map<Long, TreeMap<Long, Position>> positionHistory) {
		cache.put(sessionId, positionHistory);
	}


	/**
	 * Returns position history data starting at the specified timestamp for a given session with the specified session ID
	 * 
	 * @return the {@link PositionHistory} for a given session ID and timeStamp.
	 */
	public Optional<List<PositionHistory>> getHistory(long sessionId, long timeStamp) {
		
		// If cache contains the session
		if (cache.containsKey(sessionId)) {
			Map<Long, TreeMap<Long, Position>> positionHistories = cache.get(sessionId);
			List<PositionHistory> histories = new LinkedList<>();
			long positionHistoryChunkLength = positionHistoryCacheConfiguration.getPositionHistoryChunkLength();
			
			for (Entry<Long, TreeMap<Long, Position>> entry : positionHistories.entrySet()) {
				
				PositionHistory positionHistory = new PositionHistory();
				positionHistory.personId = entry.getKey();
				
				Long endTime;
				Long startTime;
				
				Entry<Long, Position> startingEntry = entry.getValue().ceilingEntry(timeStamp);
				if (startingEntry == null) {
					endTime = entry.getValue().lastKey();
					startTime = entry.getValue().ceilingKey(endTime - positionHistoryChunkLength);
				} else {
					startTime = startingEntry.getKey();
					endTime = entry.getValue().floorKey(startTime + positionHistoryChunkLength);
				}
				
				entry.getValue().subMap(startTime, true, endTime, true).entrySet().forEach(e -> {
					Position position = new Position();
					position.timeStamp = e.getValue().timeStamp;
					position.x = e.getValue().x;
					position.y = e.getValue().y;
					positionHistory.positions.add(position);
				});
				
				histories.add(positionHistory);
			}
			
			return Optional.of(histories);
		} else {
			// Cache does not contain session, no data
			return Optional.empty();
		}
	}
}