package com.thecoachx.coachx.position;

/**
 * Represents one position snapshot, including timestamp
 * 
 * @author Matt
 */
public class Position {
	public long timeStamp;
	public double x;
	public double y;
}
