package com.thecoachx.coachx.position;

/**
 * Configuration class for position history related functionality
 * 
 * @author Matt
 */
public class SessionPositionHistoryCacheConfiguration {

	/** Represents the time, in milliseconds, of the length of each position history "clip" that is loaded/sent */
	private long positionHistoryChunkLength;

	
	public long getPositionHistoryChunkLength() {
		return positionHistoryChunkLength;
	}
	

	public void setPositionHistoryChunkLength(long positionHistoryChunkLength) {
		this.positionHistoryChunkLength = positionHistoryChunkLength;
	}
}