package com.thecoachx.coachx.position;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Service for providing position data to REST position services
 * 
 * @author Matt
 */
@Singleton
public class SessionPositionLoader {
	
	private SessionPositionHistoryCache positionHistoryCache;

	@Inject
	SessionPositionLoader(SessionPositionHistoryCache positionHistoryCache) {
		this.positionHistoryCache = positionHistoryCache;
	}
	
	
	/**
	 * @param sessionId to load position histories for
	 */
	public List<PositionHistory> loadPositionHistories(long sessionId, long timeStamp) {
		// Attempt to load via cache
		Optional<List<PositionHistory>> history = positionHistoryCache.getHistory(sessionId, timeStamp);
		
		if (history.isPresent()) {
			// Cache hit
			return history.get();
		} else {
			// Cache miss
		}
		
		return Lists.newLinkedList();
	}
	
	
	/**
	 * Returns the {@link PositionHistory} given a sessionId, timeStamp and personId
	 */
	public Optional<PositionHistory> loadPositionHistories(long sessionId, long timeStamp, long personId) {
		// Attempt to load via cache
		Optional<List<PositionHistory>> history = positionHistoryCache.getHistory(sessionId, timeStamp);
		
		if (history.isPresent()) {
			// Cache hit
			com.google.common.base.Optional<PositionHistory> tryFind = Iterables.tryFind(history.get(), i -> i.personId == personId);
			return tryFind.isPresent() ? Optional.of(tryFind.get()) : Optional.empty();
		} else {
			// Cache miss
		}
		
		return Optional.empty();
	}
}