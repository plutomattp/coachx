package com.thecoachx.coachx.position;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * Represents position history by person ID
 * 
 * @author Matt
 */
public class PositionHistory {
	public long personId;
	public List<Position> positions = Lists.newLinkedList();
}
