package com.thecoachx.coachx.position.rest.v1;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;
import com.thecoachx.coachx.position.PositionHistory;
import com.thecoachx.coachx.position.SessionPositionLoader;

import spark.Route;

/**
 * @author Matt
 */ 
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="Provides the position history for all players in a specified session, the timestamp represents the start time of the position history, measured in the number milliseconds since the session started.",
	exampleResponse="[\n"
				  +	"  {\n"
			      + "    personId: 1,\n"
			      + "    positions: [\n"
			      + "      {timeStamp: 2000, x: 1.2, y: 3.5},\n"
			      + "      {timeStamp: 2125, x: 1.3, y: 3.4},\n"
			      + "      ...\n"
			      + "    ]\n"
			      + "  },\n"
			      + "  {\n"
			      + "    personId: 2,\n"
			      + "    ...\n"
			      + "  },\n"
			      + "  ...\n"
			      + "]"
)
public class GetPositionHistoryBySession implements RestGetEndpoint {
	
	private SessionPositionLoader sessionPositionLoader;

	@Inject
	GetPositionHistoryBySession(SessionPositionLoader sessionPositionLoader) {
		this.sessionPositionLoader = sessionPositionLoader;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/positionHistory/:sessionid/:timestamp";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			Gson gson = new Gson();
			JsonArray responseArray = new JsonArray();
			
			long sessionId = Long.parseLong(req.params(":sessionid"));
			long timestamp = Long.parseLong(req.params(":timestamp"));
			
			List<PositionHistory> loadedPositionHistories = sessionPositionLoader.loadPositionHistories(sessionId, timestamp);
			
			loadedPositionHistories.forEach(positionHistory -> {
				responseArray.add(gson.toJsonTree(positionHistory));
			});
			
			return new Gson().toJson(responseArray);
		};
	}
}