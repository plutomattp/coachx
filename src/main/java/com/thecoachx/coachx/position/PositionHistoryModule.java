package com.thecoachx.coachx.position;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.thecoachx.coachx.core.ConfigurationParser;

/**
 * DI module for PositionHistory
 * 
 * @author Matt
 */
public class PositionHistoryModule implements Module {

	@Override
	public void configure(Binder binder) {
	}

	
	@Provides
	@Singleton
	public SessionPositionHistoryCacheConfiguration providePositionHistoryCacheConfiguration(ConfigurationParser configurationParser) {
		return configurationParser.read(SessionPositionHistoryCacheConfiguration.class);
	}
}