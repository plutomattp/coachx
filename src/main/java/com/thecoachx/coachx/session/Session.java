package com.thecoachx.coachx.session;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.JsonObject;

/**
 * Represents a session/game
 * 
 * @author Matt
 */
public class Session {
	
	/** Unique session ID */
	private long sessionId;
	
	/** The start time of the session, in UNIX time */
	private long startTimeStamp;
	
	/** Location of the session */
	private String location;
	
	/** Type of the session, Football, Lacrosse etc. */
	private JsonObject sportField;
	
	/** Players associated with this session */
	private final List<Long> players = new LinkedList<>();
	
	
	public Date getStartDate() {
		return new Date(startTimeStamp);
	}

	
	public void setStartDate(Date startDate) {
		this.startTimeStamp = startDate.getTime();
	}


	public List<Long> getPlayers() {
		return players;
	}


	public long getSessionId() {
		return sessionId;
	}


	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public JsonObject getSportField() {
		return sportField;
	}


	public void setSportField(JsonObject sportField) {
		this.sportField = sportField;
	}
}