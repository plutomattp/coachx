package com.thecoachx.coachx.session.rest.v1;

import java.util.Date;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.thecoachx.coachx.core.persistence.access.SessionAccess;
import com.thecoachx.coachx.core.persistence.schema.Session;
import com.thecoachx.coachx.core.rest.Documentation;
import com.thecoachx.coachx.core.rest.Error;
import com.thecoachx.coachx.core.rest.RestGetEndpoint;
import com.thecoachx.coachx.core.rest.Use;
import com.thecoachx.coachx.core.rest.middleware.ApiInvocationLogger;

import spark.Route;

/**
 * @author Matt
 */
@Use({
	ApiInvocationLogger.class 
})
@Documentation(
	description="Provides information related to a session",
	exampleResponse="{\n"
			      + "  sessionId: 1,\n"
			      + "  startTimeStamp: 1478736000000,\n"
			      + "  location: \"Sunset Valley Highshool\",\n"
			      + "  sportField: {\n"
			      + "    fieldType: \"FOOTBALL\",\n"
			      + "    length: 109.728,\n"
			      + "    height: 48.768\n"
			      + "  },\n"
			      + "  players: [\n"
			      + "    1,\n"
			      + "    2,\n"
			      + "    3\n"
			      + "  ]\n"
			      + "}\n"
)
public class GetSession implements RestGetEndpoint {
	
	private SessionAccess sessionAccess;

	@Inject
	GetSession(SessionAccess sessionAccess) {
		this.sessionAccess = sessionAccess;
	}

	
	@Override
	public String getPath() {
		return "/api/v1/session/:sessionId";
	}

	
	@Override
	public Route getRoute() {
		return (req, res) -> {
			long sessionId = 0L;
			
			try {
				sessionId = Long.parseLong(req.params(":sessionId"));
			} catch (Exception e) {
				return new Gson().toJson(Error.error("Invalid session ID", "Invalid session ID"));
			}
			
			Optional<Session> sessionById = sessionAccess.getSessionById(sessionId);
			
			if (sessionById.isPresent()) {
				com.thecoachx.coachx.session.Session session = new com.thecoachx.coachx.session.Session();
				Session persistentSession = sessionById.get();
				session.setStartDate(new Date(persistentSession.getStartTime()));
				session.getPlayers().addAll(persistentSession.getPersons());
				session.setLocation(persistentSession.getLocation());
				session.setSessionId(persistentSession.getSessionId());
				session.setSportField(persistentSession.getSportField().getJsonRepresentation());
				
				return new Gson().toJson(session);
			} else {
				return new Gson().toJson(Error.error("Invalid session ID", "No session found with ID: [" + sessionId + "]"));
			}
		};
	}
}