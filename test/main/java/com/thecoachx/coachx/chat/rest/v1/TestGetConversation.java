package com.thecoachx.coachx.chat.rest.v1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.thecoachx.coachx.core.persistence.access.ConversationAccess;
import com.thecoachx.coachx.core.persistence.schema.ChatMessage;
import com.thecoachx.coachx.core.persistence.schema.Conversation;

import spark.Request;
import spark.Response;

/**
 * Test class for {@link GetConversation}
 * 
 * @author Matt
 */
public class TestGetConversation {

	@Mock
	private ConversationAccess conversationAccess;
	
	private GetConversation getConversation;
	
	
	@Before
	public void setup() {
		initMocks(this);
		getConversation = new GetConversation(conversationAccess);
	}
	
	
	/**
	 * Tests the REST API path is as expected
	 */
	@Test
	public void testGetPath() {
		assertEquals("/api/v1/conversation/:conversationid", getConversation.getPath());
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testGetConversation() throws Exception {
		Request mockRequest = mock(Request.class);
		Response mockResponse = mock(Response.class);
		
		String conversationIdParamName = ":conversationid";
		String conversationId = "324166";
		
		when(mockRequest.params(conversationIdParamName)).thenReturn(conversationId);
		when(conversationAccess.findById(Long.parseLong(conversationId))).thenReturn(Optional.of(getConversation(conversationId)));
		
		String response = (String) getConversation.getRoute().handle(mockRequest, mockResponse);
		
		JsonObject responseObject = new Gson().fromJson(response, JsonObject.class);
		
		assertEquals(3, responseObject.get("participants").getAsJsonArray().size());
		assertTrue(responseObject.get("participants").getAsJsonArray().contains(new JsonPrimitive(1)));
		assertTrue(responseObject.get("participants").getAsJsonArray().contains(new JsonPrimitive(2)));
		assertTrue(responseObject.get("participants").getAsJsonArray().contains(new JsonPrimitive(3)));
		
		assertEquals(4, responseObject.get("messages").getAsJsonArray().size());
		
		assertTrue(
			Iterables.tryFind(
				responseObject.get("messages").getAsJsonArray(), 
				m -> { 
					return m.getAsJsonObject().get("sender").getAsLong() == 1 &&
						   m.getAsJsonObject().get("message").getAsString().equals("Message 1") &&
						   m.getAsJsonObject().get("timestamp").getAsLong() == 0l; 
				}
			).isPresent()
		);
		
		assertTrue(
			Iterables.tryFind(
				responseObject.get("messages").getAsJsonArray(), 
				m -> { 
					return m.getAsJsonObject().get("sender").getAsLong() == 2 &&
						   m.getAsJsonObject().get("message").getAsString().equals("Message 2") &&
						   m.getAsJsonObject().get("timestamp").getAsLong() == 1000l; 
				}
			).isPresent()
		);
		
		assertTrue(
			Iterables.tryFind(
				responseObject.get("messages").getAsJsonArray(), 
				m -> { 
					return m.getAsJsonObject().get("sender").getAsLong() == 1 &&
						   m.getAsJsonObject().get("message").getAsString().equals("Message 3") &&
						   m.getAsJsonObject().get("timestamp").getAsLong() == 2000l; 
				}
			).isPresent()
		);
		
		assertTrue(
			Iterables.tryFind(
				responseObject.get("messages").getAsJsonArray(), 
				m -> { 
					return m.getAsJsonObject().get("sender").getAsLong() == 3 &&
						   m.getAsJsonObject().get("message").getAsString().equals("Message 4") &&
						   m.getAsJsonObject().get("timestamp").getAsLong() == 5000l; 
				}
			).isPresent()
		);
	}


	private Conversation getConversation(String conversationId) {
		Conversation conversation = new Conversation();
		conversation.setConversationId(Long.parseLong(conversationId));
		
		conversation.getParticipants().add(1L);
		conversation.getParticipants().add(2L);
		conversation.getParticipants().add(3L);
		
		conversation.getMessages().add(chatMessage("Message 1", 1, 0));
		conversation.getMessages().add(chatMessage("Message 2", 2, 1000));
		conversation.getMessages().add(chatMessage("Message 3", 1, 2000));
		conversation.getMessages().add(chatMessage("Message 4", 3, 5000));
		
		return conversation;
	}
	
	
	private ChatMessage chatMessage(String message, long senderId, long timeStamp) {
		ChatMessage chatMessage = new ChatMessage();
		
		chatMessage.setMessage(message);
		chatMessage.setSenderId(senderId);
		chatMessage.setTimeStamp(timeStamp);
		
		return chatMessage;
	}
}